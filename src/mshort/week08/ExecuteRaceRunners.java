package mshort.week08;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteRaceRunners {

    public static void main(String[] args) {
        int distance = 30;
        ExecutorService myService = Executors.newFixedThreadPool(4);
        RaceRunners[] myRunners = new RaceRunners[4];

        myRunners[0] = new RaceRunners("cyan", distance);
        myRunners[1] = new RaceRunners("magenta", distance);
        myRunners[2] = new RaceRunners("yellow", distance);
        myRunners[3] = new RaceRunners("black", distance);

        try {
            myService.execute(myRunners[0]);
            myService.execute(myRunners[1]);
            myService.execute(myRunners[2]);
            myService.execute(myRunners[3]);
        } catch (Exception e) {
            System.out.println("Issue Encountered Executing myRunners: " + e.toString());
        }
        myService.shutdown();
    }

}
