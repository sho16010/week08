package mshort.week08;

public class CountRunner {

    public static void main(String[] args) throws InterruptedException {

        // One Instance of ThreadNumberCounter
        ThreadNumberCounter myThreadCounter = new ThreadNumberCounter();

        // 5 Separate threads
        Thread[] myThreads = new Thread[5];
        for (int a = 0; a < myThreads.length; a++) {
            myThreads[a] = new Thread(myThreadCounter);
        }

        myThreads[0].start();
        myThreads[1].start();
        myThreads[2].start();
        myThreads[3].start();
        myThreads[4].start();

        myThreads[0].join();
        myThreads[1].join();
        myThreads[2].join();
        myThreads[3].join();
        myThreads[4].join();

        // This will execute after the above threads finish because of join()
        System.out.println("Total Count is [" + myThreadCounter.totalCount + "]");
    }
}
