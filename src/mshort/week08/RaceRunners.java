package mshort.week08;

import java.util.Random;

public class RaceRunners implements Runnable {

    private final String color;
    private int distancePerInterval;
    private int restPeriod;
    private final int totalDistance;
    private int time;

    public RaceRunners (String a_color, int a_totalDistance) {
        Random myRandoms = new Random();

        this.color = a_color;
        this.totalDistance = a_totalDistance;
        this.time = 0;
        this.distancePerInterval = myRandoms.nextInt(10);
        this.restPeriod = myRandoms.nextInt(5);

        // Perform lower bounds check and correct restPeriod and distancePerInterval
        if (restPeriod <= 0) {
            restPeriod = 1;
        }
        if (distancePerInterval <= 0) {
            distancePerInterval = 1;
        }
    }

    public void run() {
        int distanceCovered = 0;
        System.out.println("[" + color + "] Starting, totalDistance [" + totalDistance + "], distancePerInterval [" + distancePerInterval + "], restPeriod [" + restPeriod + "]");
        while (distanceCovered < totalDistance) {
            distanceCovered = distanceCovered + distancePerInterval;
            time = time + 1; // One interval each iteration
            if (distanceCovered < totalDistance) {
                try {
                    // System.out.println("[" + color + "] resting");
                    time = time + restPeriod; // add restPeriod intervals to time
                    Thread.sleep(restPeriod); // wait
                } catch (InterruptedException e) {
                    System.out.println("Exception Encountered during restPeriod: " + e.toString());
                }
            }
        }
        System.out.println("[" + color + "] Finished, totalDistance [" + totalDistance + "], time [" + time + "]");
    }

    public static void main(String[] args) {
        int distance = 30;
        RaceRunners[] myRunners = new RaceRunners[4];

        myRunners[0] = new RaceRunners("cyan", distance);
        myRunners[1] = new RaceRunners("magenta", distance);
        myRunners[2] = new RaceRunners("yellow", distance);
        myRunners[3] = new RaceRunners("black", distance);

        Thread[] myThreads = new Thread[4];
        myThreads[0] = new Thread(myRunners[0]);
        myThreads[1] = new Thread(myRunners[1]);
        myThreads[2] = new Thread(myRunners[2]);
        myThreads[3] = new Thread(myRunners[3]);

        myThreads[0].start();
        myThreads[1].start();
        myThreads[2].start();
        myThreads[3].start();

    }

}
