package mshort.week08;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadNumberCounter extends Thread {

    AtomicInteger totalCount;

    ThreadNumberCounter()
    {
        totalCount = new AtomicInteger();
    }

    public void run() {
        Random myRandom = new Random();
        int waitPeriod = myRandom.nextInt(333);
        int localCount = 0;

        // No zero waitPeriod
        if (waitPeriod <= 0) {
            waitPeriod = 1;
        }
        System.out.println("ThreadNumberCounter Starting...");
        while (localCount < 100) {
            try {
                Thread.sleep(waitPeriod); // wait, so they do not all end at same time
            } catch (InterruptedException e) {
                System.out.println("Exception Encountered during waitPeriod: " + e.toString());
            }
            totalCount.addAndGet(1);
            localCount++;
        }
        System.out.println("ThreadNumberCounter Finished local [" + localCount + "] total (Atomic) [" + totalCount + "]");
    }
}

